﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dict
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array = { 1, 1, 2, 4, 5, 5, 7, 7, 2, 4, 9 };
            Console.WriteLine(FindUniqueNumber(array));
            Console.ReadLine();
        }
        public static int FindUniqueNumber(int[] array)
        {
            var dictionary = new Dictionary<int, int>();
            for (int i = 0; i < array.Length; i++)
            {
                if (dictionary.ContainsKey(array[i]))
                    dictionary.Remove(array[i]);
                else
                    dictionary.Add(array[i], 1);
            }
            return dictionary.First().Key;
        }
    }
}
